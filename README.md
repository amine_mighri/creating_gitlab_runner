# References -------------------------------------------------------------------------------
# https://docs.gitlab.com/runner/install/docker.html
# https://docs.gitlab.com/runner/register/index.html#docker


# Steps ------------------------------------------------------------------------------------
#1. Install Docker


# We need to create a volume on host machine and then map Docker container location to that volume.
# It will help us to persist the data on host machine.
# Once Docker containers has been removed from Docker Hub/Repository,
# the changes within those containers will be removed as well.

# To run gitlab-runner inside a Docker container,
# you need to make sure that the configuration is not lost when the container is restarted.
# To do this we need to create volumes in either of the below 2 ways,

#2. Execute the command below if you dont want to create a volume,
	# Windows
	docker run -d --name gitlab-runner --restart always -v E:\Repository\GitLab\config:/etc/gitlab-runner -v
	E:\Repository\GitLab\docker.sock:/var/run/docker.sock gitlab/gitlab-runner
	
	# Linux
	docker run -d --name gitlab-runner --restart always -v /home/nayan/Documents/Repository/Docker_Volume/config:/etc/gitlab-runner 	-v /home/nayan/Documents/Repository/Docker_Volume/docker.sock:/var/run/docker.sock gitlab/gitlab-runner

#OR

#2. If you want to create a volume and then want to map that volume then run below command,
	# Default Location,
		# For Windows
		\wsl$\docker-desktop-data\version-pack-data\community\docker\volumes\
		
		# For Linux
		# You can find the location using
		# docker volume inspect volume_name
	docker volume create gitlab-runner-config
	docker run -d --name gitlab-runner --restart always -v gitlab-runner-config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/	docker.sock gitlab/gitlab-runner


# As we are running the Docker GitLab-Runner with mounted volume from host,
# the configuration file will be saved to the specified volume location on host.
#3. Register the GitLab Runner installed in Docker.
	# Windows	
	docker run --rm -it -v E:\Repository\GitLab\config:/etc/gitlab-runner gitlab/gitlab-runner register

	# Linux
	docker run --rm -it -v /home/nayan/Documents/Repository/Docker_Volume/config:/etc/gitlab-runner gitlab/gitlab-runner register
	
	
	# OR with volume create from above 2.OR option
	docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner register


#4. Now you can run the GitLab-Runner commands,
	gitlab-runner <runner command and options...>
   
   As below from Docker GitLab-Runner,
	docker run <chosen docker options...> gitlab/gitlab-runner <runner command and options...>
	docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner --help


#5. In case if you want to stop and remove the container,
	docker stop gitlab-runner # Because we have provided container name as gitlab-runner, we can provide 		
							  # container id as well.
	docker rm gitlab-runner	# Because we have provided container name as gitlab-runner, we can provide 		
							  # container id as well.
